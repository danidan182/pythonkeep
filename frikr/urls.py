"""frikr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from photos import views as photos_views
from photos.api import PhotoListAPI
from users import views as users_views
from users.api import UserListAPI, UserDetailAPI
from users.views import LoginView, LogoutView
from photos.views import HomeView, DetailView, CreateView, PhotoListView, UserPhotosView
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # photos URLS
    url(r'^$', HomeView.as_view(), name='photos_home'),
    url(r'^photos/(?P<pk>[0-9]+)', DetailView.as_view(), name='photos_detail'),
    url(r'^photos/new$', CreateView.as_view(), name='create_photo'),
    url(r'^photos/$', PhotoListView.as_view(), name='list_photo'),
    # se puede proteger la url bajo login mediante este decorador en url, pero es mas elegante usar una clase base
    url(r'^my-photos/$', login_required(UserPhotosView.as_view()), name='user_photos'),

    # photos API URLs
    url(r'^api/1.0/photos/$',PhotoListAPI.as_view(), name='photo_list_api'),

    # users URLS
    url(r'^login$', LoginView.as_view(), name='users_login'),
    url(r'^logout$', LogoutView.as_view(), name='users_logout'),

    # users API URLs
    url(r'^api/1.0/users/$', UserListAPI.as_view(), name='user_list_api'),
    url(r'^api/1.0/users/(?P<pk>[0-9]+)$', UserDetailAPI.as_view(), name='user_detail_api')

]
