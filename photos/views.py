# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse,HttpResponseNotFound
from django.shortcuts import render

from photos.forms import PhotoForm
from photos.models import Photo, PUBLIC
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from photos.settings import COPYLEFT
from django.views.generic import View, ListView
from django.utils.decorators import method_decorator
from django.db.models import Q


class PhotosQuerySet(object):

    def get_photos_queryset(self, request):
        if not request.user.is_authenticated():
            photos = Photo.objects.filter(visibility=PUBLIC)
        elif request.user.is_superuser:
            photos = Photo.objects.all()
        else:
            # https://docs.djangoproject.com/en/1.11/topics/db/queries/#complex-lookups-with-q-objects
            photos = Photo.objects.filter(Q(owner=request.user) | Q(visibility=PUBLIC))

        return photos

class HomeView(View):

    def get(self, request):
        """
        Esta función devuelve  el home de mi página
        """
        photos = Photo.objects.filter(visibility=PUBLIC, license=COPYLEFT).order_by('-created_at')
        context = {
            'photos_list': photos[:4]
        }

        return render(request, 'photos/home.html', context)

class DetailView(View, PhotosQuerySet):
    def get(self, request, pk):
        """
        Carga la página de detalle de una foto
        :param request: HttpRequest
        :param pk: id de la foto
        :return: HttpResponse
        """
        possible_photos = self.get_photos_queryset(request).filter(pk=pk).select_related('owner')  # el select_related es para hacer el join a owner y ya poseerlo a la hora de printar
        photo = possible_photos[0] if len(possible_photos) == 1 else None

        if photo is not None:
            # cargar la plantilla de detalle
            context = {
                'photo': photo
            }
            return render(request, 'photos/detail.html', context)
        else:
            return HttpResponseNotFound('No existe la foto') # 404 - Not found

class CreateView(View):

    #login_required es un decorador basado en vistas de funciones, no en métodos de clase
    @method_decorator(login_required())
    def get(self, request):
        """
        Muestra un formulario para crear una foto
        :param request: HttpRequest
        :return: HttpResponse
        """

        form = PhotoForm()

        context = {
            'create_form': form,
            'success_message': ''
        }
        return render(request, 'photos/new_photo.html', context)

    @method_decorator(login_required())
    def post(self, request):
        """
        Crea una foto en base a la información POST
        :param request: HttpRequest
        :return: HttpResponse
        """
        success_message = ''

        photo_with_owner = Photo()
        photo_with_owner.owner = request.user  # asigno como propietario de la foto el usuario autenticado
        form = PhotoForm(request.POST, instance=photo_with_owner)
        if form.is_valid():
            new_photo = form.save()
            form = PhotoForm()
            success_message = 'Guardado con éxito!'
            success_message += '<a href="{0}">'.format(reverse('photos_detail', args=[new_photo.pk]))
            success_message += 'Ver foto'
            success_message += '</a>'

        context = {
            'create_form': form,
            'success_message': success_message
        }
        return render(request, 'photos/new_photo.html', context)

class PhotoListView(View, PhotosQuerySet):

    def get(self, request):
        """Devuelve:
        - Las fotos publicas si el usuario no está autenticado
        - Las fotos del usuario autenticado o públicas de otros
        - Si el usuario es superadministrador, todas las fotos
        :param request: HttpRequest
        :return HttpResponse
        """


        # obtener fotos cuyo owner tiene como nombre Daniel
        # photos = Photo.objects.filter(owner__first_name='Daniel')

        context = {
            'photos': self.get_photos_queryset(request)
        }

        return render(request, 'photos/photos_list.html',context)

class UserPhotosView(ListView):

    model = Photo
    template_name = 'photos/user_photos.html'

    def get_queryset(self):
        queryset = super(UserPhotosView, self).get_queryset()
        return queryset.filter(owner=self.request.user)
